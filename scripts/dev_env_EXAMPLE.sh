#!/bin/bash

# You should copy and paste this entire file to `dev_env.{sh,bat}` for development, filling out the variables below for your specific use case. Then, during development just run this file prior to starting the uvicorn server to set all the environment variables correctly.

# Set DEBUG to `True` for any development or debugging. In production this will automatically be set to `False`.
export DEBUG="True"

# Logging levels, adjust as needed.
export LOG_LEVEL="DEBUG"

# Set your database URL here. There are free Postgres-as-a-service providers available. If none is provided, defaults to a SQLite database.
export DATABASE_URL="postgres://USERNAME:PASSWORD@SERVER_URL:5432/app"

# This is the secret value used to secure your HMAC-signed cookies and authenticate users. Make sure it's long and random, even for development. A good random string can be generated with `openssl rand -hex 32`.
export SECRET="SOME_RANDOM_STRING"

#################################################################################################################
# Windows (.bat) Version
#set DEBUG=True