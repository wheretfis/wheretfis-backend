# wheretfis-backend
Lazy asset inventory management. This part is the core backend server.

## Deployment
### Docker (Easy)
1. `docker-compose up -d`

### Manual (Hard)
Coming soon.

## Contributing
### Development Environment
The following describes how to get a local instance of WTFis Server setup for developers. If any step is unclear, or results in errors, please open an issue with the `documentation` tab, and describe where improvements can be made.

#### First Time Setup
1. Install [python3 with pip](https://www.python.org/), and an optional [PostgreSQL](https://www.postgresql.org/) server. In theory the server should run on all major operating systems (Windows, macOS, and \*nix), so refer to the install instructions for your OS for each software package. If you don't feel like self-hosting a PostgreSQL server, there are several "PostgreSQL-as-a-Service" providers available, which do offer "Free" tiers perfect for development. Alternatively, this should support [SQLite](https://www.sqlite.org/index.html) out of the box (just ensure the DATABASE_URL env variable is blank or does not exist). Either way, the database should be empty, and you only need one user with read/write access.
2. `pip install -g virtualenv` - Install [virtualenv](https://virtualenv.pypa.io/en/latest/) onto your host machine. Virtualenv allows you to separate an apps Python dependencies from your system dependencies, and while it's not required, it's highly recommended.
3. Clone this repo to your local machine.
4. `cd wheretfis` - Enter the repo directory.
5. `virtualenv --python python3 venv` - Create a new virtualenv for the backend. If you get an error about "python3" not existing, just use "python" instead, as long as you're sure that it's version 3 (no Python version 2 support).
6. `source venv/scripts/activate.sh`(macOS and \*nix) or `venv\Scripts\activate.bat`(Windows) - This activates your virtualenv, making it so that calls to `python` or `pip` will only affect this app, and not your overall host machine.
7. `pip install -r requirements.txt` - Installs most dependencies.
8. `pip install -r requirements-dev.txt` - Installs development dependencies.
9. `cd scripts` - Enter the `scripts` directory.
10. `cp dev_env_EXAMPLE.sh dev_env.sh` - Create a new dev_env file. The server gets it's configuration from environment variables, and this file sets your environment variables for development. Also, if you're on Windows, you'll need to change it to a .bat file, and use the "Windows" syntax at the bottom to set the environment variables correctly.
11. Change the values in the `dev_env` file to correctly match your setup. The docs for each one should tell you what each variables does, and what it should be. Save and exit when everything looks correct.
12. Run the `dev_env` file to set all your environment variables in your current terminal session (you need it for the database upgrade).
13. `cd ..` - Move back to the top directory.
14. `alembic upgrade HEAD` - This runs [Alembic](https://alembic.sqlalchemy.org/en/latest/) to automatically make the tables into the correct format in your database. In general Alembic should manage all your database migrations, so you shouldn't need to manually change anything about the database.

#### Running the Instance
1. `source venv/scripts/activate.sh`(macOS and \*nix) or `venv\Scripts\activate.bat`(Windows) - This activates your virtualenv, making it so that calls to `python` or `pip` will only affect this app, and not your overall host machine.
2. `scripts/dev_env.sh` - Run your `dev_env` file to set all your environment variables for development in this terminal.
3. `python -m app` - This starts a [uvicorn](https://www.uvicorn.org/) server with automatic reloading so changes to files will be reflected immediately (without having to stop and restart the server).
4. Point a web browser at http://localhost:5000. To access the automatically generated API docs (plus a sweet integrated client), go to http://localhost:5000/docs (OpenAPI/ Swagger) or http://localhost:5000/redoc (ReDoc).

#### Misc Development Tasks
* **Linters** - The app has style guides integrated, and linters will run automatically on each merge request. Nevertheless, if you want to check your code before commiting it you can.
    * From the `wheretfis-backend` directory, run `flake8`.
* **DB Migrations** - If you make changes to the backend models that require a database migration (adding a new model, changing a column, etc.), you'll need to make migrations for it. Luckily, Alembic will autogenerate most migrations for you. Just run `alembic revision --autogenerate -m "DESCRIBE CHANGE HERE"` to generate the migration, then `alembic upgrade HEAD` to change your current database to match the new format.
