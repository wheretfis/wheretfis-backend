import app.deps
from app.models.user import User
from app.security import create_token

from fastapi import APIRouter, Depends, HTTPException, status
from fastapi.security import OAuth2PasswordRequestForm
from sqlalchemy.orm import Session
from pydantic import BaseModel

router = APIRouter()


class OAuth2Token(BaseModel):
    access_token: str
    token_type: str


@router.post("/login", response_model=OAuth2Token)
def login(form_data: OAuth2PasswordRequestForm = Depends(),
          db: Session = Depends(app.deps.get_db)):
    user_obj = db.query(User).filter(User.email == form_data.username).first()

    # Technically there's a slight security issue here where you can (possibly) infer if a user
    # exists based on the timing of the response. It's low-risk for now.
    if not user_obj or not user_obj.is_password_correct(form_data.password):
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Incorrect username or password.",
            headers={"WWW-Authenticate": "Bearer"},
        )
    return {"access_token": create_token(user_obj), "token_type": "bearer"}
