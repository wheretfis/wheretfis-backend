from typing import List
from app.deps import get_db
from app.models.asset_tag import AssetTag
from app.models.item import Item, ItemSchema, NewItemSchema, ShortItemSchema, UpdateItemLocationSchema, UpdateItemSchema
from app.models.location_history import LocationHistory
from app.models.user import User, ShortUserSchema
from app.models.user_history import UserHistory
from app.security import get_current_user, get_item_obj_by_asset_tag

from fastapi import APIRouter, Depends, HTTPException, status
from sqlalchemy.exc import SQLAlchemyError
from sqlalchemy.orm import Session
from loguru import logger

router = APIRouter()


def get_item_full(item_obj: Item, db: Session) -> dict:
    # Get the new object so we can return it.
    tmp_obj = ItemSchema.from_orm(item_obj).dict()

    # Get the list of users who've had this, and find the latest one.
    user_history_objs = db.query(UserHistory).filter(UserHistory.item_id == item_obj.id).all()
    # Sort it by creation time, from most recent to earliest
    user_history_objs.sort(key=lambda x: x.creation_time, reverse=True)

    user_obj = db.query(User).filter(User.id == user_history_objs[0].new_user_id).first()
    tmp_obj["last_user"] = ShortUserSchema.from_orm(user_obj).dict()

    # Get the last location
    location_history_obj = db.query(LocationHistory).filter(LocationHistory.item_id == item_obj.id).all()
    # Sort it by creation time, from most recent to earliest
    location_history_obj.sort(key=lambda x: x.creation_time, reverse=True)

    if location_history_obj[0].new_container_item_id is None:
        tmp_obj["parent_container"] = None
    else:
        tmp_obj["parent_container"] = ShortItemSchema
        tmp_obj["parent_container"].id = location_history_obj[0].new_container_item_id
        tmp_parent_obj = db.query(Item).filter(Item.id == location_history_obj[0].new_container_item_id).first()
        tmp_obj["parent_container"].description = tmp_parent_obj.description

        # Get all asset tags for the parent container
        tmp_obj["parent_container"].asset_tags = []
        parent_asset_tags_list = db.query(AssetTag).filter(AssetTag.item_id == tmp_parent_obj.id).all()

        for parent_asset_tag_obj in parent_asset_tags_list:
            tmp_obj["parent_container"].asset_tags.append(parent_asset_tag_obj.id)

    # Get all asset tags
    tmp_obj["asset_tags"] = []
    asset_tags_list = db.query(AssetTag).filter(AssetTag.item_id == item_obj.id).all()

    for asset_tag_obj in asset_tags_list:
        tmp_obj["asset_tags"].append(asset_tag_obj.id)

    return tmp_obj


def get_items_in(item, db, recurse):
    # Alright, so getting a listing of everything INSIDE of another container is a bit non-trivial, so here's the
    # workflow. First, we go through and get a listing of all things with a parent container that matches the top
    # level item. Some of these may be out of date. Then, we go through and cull all of these to see what the last
    # known location of any mathced ID was, to see if it still shows up as being owned by the original item. If so,
    # we add it, if not, we toss it.

    # Get all location histories that have the parent container
    location_obj_list = db.query(LocationHistory).filter(LocationHistory.new_container_item_id == item.id).all()

    possible_item_ids = []
    item_obj_list = []
    tmp_arr = []

    # Get all possible item IDs
    for location_obj in location_obj_list:
        # Prevent duplicates
        if location_obj.item_id not in possible_item_ids:
            possible_item_ids.append(location_obj.item_id)

    for item_id in possible_item_ids:
        item_obj = db.query(Item).filter(Item.id == item_id).first()
        full_item_dict = get_item_full(item_obj, db)
        if full_item_dict["parent_container"] is not None and full_item_dict["parent_container"].id == item.id:
            item_obj_list.append(item_obj)

    for item_obj in item_obj_list:
        tmp_arr.append(get_item_full(item_obj, db))

        if recurse:
            tmp_arr = tmp_arr + get_items_in(item_obj, db, recurse)

    return tmp_arr


def item_active_check(item_obj):
    if not item_obj.is_active:
        raise HTTPException(status.HTTP_400_BAD_REQUEST, detail=f"'{item_obj.description}' has been removed,"
                            " and is unavailable.")


def validate_parent_container(parent_container_asset_tag_str: str, db) -> Item:
    parent_container_obj = get_item_obj_by_asset_tag(parent_container_asset_tag_str, db)

    if parent_container_obj is None:
        raise HTTPException(status.HTTP_400_BAD_REQUEST, detail="An item with asset tag "
                            f"'{parent_container_asset_tag_str}' does not exist.")

    item_active_check(parent_container_obj)

    if not parent_container_obj.is_container:
        raise HTTPException(status.HTTP_400_BAD_REQUEST, detail=f"'{parent_container_obj.description}' "
                            "is not a container")
    return parent_container_obj


def move_item(db, current_user, item_obj, update_item_location_data: UpdateItemLocationSchema):
    # If we were inclined, this would be a good spot to check to see if the item being updated is flagged as a
    # physical place, and disallow updates to it. It's not done this way right now so that updates to things like
    # longitude and latitude can be made, even though other updates may not always make sense.
    new_location_history_obj = LocationHistory(item_obj.id)

    # First update the new container (which can be nothing, meaning it is in someone's possession)
    if not update_item_location_data.parent_container_asset_tag_id or \
            update_item_location_data.parent_container_asset_tag_id == "":
        new_location_history_obj.new_container_item_id = None
    else:
        # Parent container validation checks
        parent_container_obj = validate_parent_container(update_item_location_data.parent_container_asset_tag_id, db)
        new_location_history_obj.new_container_item_id = parent_container_obj.id

    if update_item_location_data.latitude and update_item_location_data.longitude:
        new_location_history_obj.new_latitude = update_item_location_data.latitude
        new_location_history_obj.new_longitude = update_item_location_data.longitude
    db.add(new_location_history_obj)

    # Next update the user history, if the last user has changed
    # Get the list of users who've had this, and find the latest one.
    user_history_objs = db.query(UserHistory).filter(UserHistory.item_id == item_obj.id).all()
    # Sort it by creation time, from most recent to earliest
    user_history_objs.sort(key=lambda x: x.creation_time, reverse=True)

    # Check to see who had it last.
    if user_history_objs[0].id != current_user.id:
        new_user_history_obj = UserHistory(item_obj.id, current_user.id)
        db.add(new_user_history_obj)

    db.commit()


def validate_asset_tag_list(asset_tag_str_list: List[str], db, item_obj: Item = None):
    # First ensure we have at least one asset tag in the list.
    if len(asset_tag_str_list) < 1:
        raise HTTPException(status.HTTP_400_BAD_REQUEST,
                            detail="At least one asset tag is required for items.")

    # Check the validity of the asset tags
    for asset_tag_str in asset_tag_str_list:
        # Correctness
        if "/" in asset_tag_str:
            raise HTTPException(status.HTTP_400_BAD_REQUEST, detail="Asset tags cannot contain slashes.")

        # Ensure no one is already using them (or the current item is using them). If `item_obj`
        # is None then we can safely assume these tags should be totally new
        asset_tag_obj = db.query(AssetTag).filter(AssetTag.id == asset_tag_str).first()
        if asset_tag_obj is not None:
            if item_obj is not None and asset_tag_obj.item_id == item_obj.id:
                # This is fine
                return

            raise HTTPException(status.HTTP_400_BAD_REQUEST,
                                detail=f"Asset tag '{asset_tag_str}' already exists for another item.")


@router.get("/items/", response_model=List[ItemSchema])
def get_item_list(
        q: str = None,
        current_user: User = Depends(get_current_user),
        db: Session = Depends(get_db)):
    item_obj_list = db.query(Item)

    if q:
        item_obj_list = item_obj_list.filter(Item.description.contains(q))

    item_obj_list = item_obj_list.all()

    tmp_arr = []

    for item_obj in item_obj_list:
        tmp_arr.append(get_item_full(item_obj, db))

    return list(tmp_arr)


@router.get("/items/{asset_tag_id}", response_model=ItemSchema)
def get_item(
        item: Item = Depends(get_item_obj_by_asset_tag),
        current_user: User = Depends(get_current_user),
        db: Session = Depends(get_db)):

    return get_item_full(item, db)


# Each user can create any new item
@router.post("/items", response_model=ItemSchema, status_code=status.HTTP_201_CREATED)
def create_item(
        new_item_data: NewItemSchema,
        current_user: User = Depends(get_current_user),
        db: Session = Depends(get_db)):
    if new_item_data.is_physical_place and not new_item_data.is_container:
        raise HTTPException(status.HTTP_400_BAD_REQUEST, detail="All physical places are required to be containers.")

    validate_asset_tag_list(new_item_data.asset_tags, db, None)

    # Create the item (in a weird way since the server sets the creation time, and we can automatically create
    # it from a schema otherwise)
    new_item_obj = Item(is_container=new_item_data.is_container,
                        is_physical_place=new_item_data.is_physical_place)
    if new_item_data.description:
        new_item_obj.description = new_item_data.description
    if new_item_data.notes:
        new_item_obj.notes = new_item_data.notes
    if new_item_data.picture_data:
        new_item_obj.picture_data = new_item_data.picture_data
    db.add(new_item_obj)
    db.commit()
    db.refresh(new_item_obj)

    # Create it's first user history object
    new_user_history_obj = UserHistory(new_item_obj.id, current_user.id)
    db.add(new_user_history_obj)

    # Create it's first location history object
    new_location_history_obj = LocationHistory(new_item_obj.id)
    if new_item_data.parent_container_asset_tag_id:
        parent_container_obj = validate_parent_container(new_item_data.parent_container_asset_tag_id, db)
        new_location_history_obj.new_container_item_id = parent_container_obj.id

    if new_item_data.latitude and new_item_data.longitude:
        new_location_history_obj.new_latitude = new_item_data.latitude
        new_location_history_obj.new_longitude = new_item_data.longitude

    db.add(new_location_history_obj)
    db.commit()

    # Create all the asset tags
    for asset_tag_str in new_item_data.asset_tags:
        asset_tag_obj = AssetTag(asset_tag_str, item_id=new_item_obj.id)
        db.add(asset_tag_obj)
    db.commit()

    return get_item_full(new_item_obj, db)


# Each user can create any new item
@router.post("/items/{asset_tag_id}/update", response_model=ItemSchema, status_code=status.HTTP_200_OK)
def update_item_location(
        update_item_location_data: UpdateItemLocationSchema,
        item_obj: Item = Depends(get_item_obj_by_asset_tag),
        current_user: User = Depends(get_current_user),
        db: Session = Depends(get_db)):
    item_active_check(item_obj)
    move_item(db, current_user, item_obj, update_item_location_data)
    return get_item_full(item_obj, db)


@router.get("/items/{asset_tag_id}/contents", response_model=List[ItemSchema])
def get_item_contents(
        recurse: bool = False,
        item: Item = Depends(get_item_obj_by_asset_tag),
        current_user: User = Depends(get_current_user),
        db: Session = Depends(get_db)):
    if not item.is_container:
        raise HTTPException(status.HTTP_400_BAD_REQUEST, detail="The item "
                            f"'{item.id}' is not a container, and has no contents.")
    return list(get_items_in(item, db, recurse))


@router.delete("/items/{asset_tag_id}", status_code=status.HTTP_200_OK)
def delete_item(item_obj: Item = Depends(get_item_obj_by_asset_tag),
                current_user: User = Depends(get_current_user),
                db: Session = Depends(get_db)):
    item_active_check(item_obj)

    if item_obj.is_container:
        contained_objs = get_items_in(item_obj, db, False)

        if contained_objs:
            raise HTTPException(status.HTTP_400_BAD_REQUEST, detail=f"'{item_obj.description}' contains other items, "
                                "and cannot be deleted until they've all been moved out.")
    # Set the item inactive
    item_obj.is_active = False

    # Next, make sure it's not contained in anything.
    update_item_location_data = UpdateItemLocationSchema()
    update_item_location_data.parent_container_asset_tag_id = None
    update_item_location_data.latitude = None
    update_item_location_data.longitude = None
    move_item(db, current_user, item_obj, update_item_location_data)

    db.commit()

    return {}


@router.put("/items/{asset_tag_id}", response_model=ItemSchema, status_code=status.HTTP_200_OK)
def update_item(item_update: UpdateItemSchema,
                item_obj: Item = Depends(get_item_obj_by_asset_tag),
                current_user: User = Depends(get_current_user),
                db: Session = Depends(get_db)):
    try:
        if item_update.description is not None:
            item_obj.description = item_update.description

        if item_update.notes is not None:
            item_obj.notes = item_update.notes

        if item_update.picture_data is not None:
            if item_update.picture_data == "":
                item_obj.picture_data = None
            else:
                item_obj.picture_data = item_update.picture_data

        if item_update.asset_tags is not None:
            validate_asset_tag_list(item_update.asset_tags, db, item_obj)

            # First delete all previous asset tags (so that we only had these asset tags attached to this item)
            prev_asset_tags_list = db.query(AssetTag).filter(AssetTag.item_id == item_obj.id).all()
            for prev_asset_tag in prev_asset_tags_list:
                db.delete(prev_asset_tag)
            db.commit()

            # Now reinit all asset tags
            for asset_tag_str in item_update.asset_tags:
                asset_tag_obj = AssetTag(asset_tag_str, item_id=item_obj.id)
                db.add(asset_tag_obj)

        db.commit()
        db.refresh(item_obj)
    except SQLAlchemyError as e:
        logger.error(str(e))
        raise HTTPException(status.HTTP_500_INTERNAL_SERVER_ERROR, detail="Item update failed.")
    return get_item_full(item_obj, db)
