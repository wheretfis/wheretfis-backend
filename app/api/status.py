from fastapi import APIRouter
from pydantic import BaseModel

from app import __version__


router = APIRouter()


class StatusSchema(BaseModel):
    version: str


@router.get("/status", response_model=StatusSchema)
def status():
    return {"version": __version__}
