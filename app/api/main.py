from fastapi import APIRouter

from app.api import setup, auth, users, api_keys, status, items

api_router = APIRouter()
api_router.include_router(status.router, tags=["status"])
api_router.include_router(auth.router, tags=["auth"])
api_router.include_router(setup.router, tags=["setup"])
api_router.include_router(users.router, tags=["users"])
api_router.include_router(api_keys.router, tags=["api_keys"])
api_router.include_router(items.router, tags=["items"])
