from app.deps import get_db
from app.models.user import User
from app.models.api_key import ApiKey, ApiKeySchema
from app.security import get_current_user

from fastapi import APIRouter, Depends, status
from pydantic import BaseModel
from sqlalchemy.orm import Session
from datetime import datetime
from typing import List

router = APIRouter()


# This particular schema does NOT return the actual API key. This is by design, as the key can only be retrieved at
# creation time.
class ApiKeyRetrieveSchema(BaseModel):
    id: int
    creation_time: datetime
    key_stub: str

    class Config:
        orm_mode = True


def create_new_api_key(db, current_user):
    # Create the new key
    new_api_key_obj = ApiKey(user_id=current_user.id)

    db.add(new_api_key_obj)
    db.commit()
    db.refresh(new_api_key_obj)
    return new_api_key_obj


@router.get("/users/me/apikeys", response_model=List[ApiKeyRetrieveSchema])
def get_my_apikeys(current_user: User = Depends(get_current_user),
                   db: Session = Depends(get_db)):
    # For now, there can only be a single API key per user.
    api_key_obj = db.query(ApiKey).filter(ApiKey.user_id == current_user.id).all()

    return api_key_obj


@router.post("/users/me/apikeys", response_model=ApiKeySchema, status_code=status.HTTP_201_CREATED)
def create_new_apikey(current_user: User = Depends(get_current_user),
                      db: Session = Depends(get_db)):
    return create_new_api_key(db, current_user)


@router.delete("/users/me/apikeys", status_code=status.HTTP_204_NO_CONTENT)
def delete_all_my_apikeys(db: Session = Depends(get_db),
                          current_user: User = Depends(get_current_user)):
    # If any previous API Keys exist for this user, delete them.
    api_key_obj_list = db.query(ApiKey).filter(ApiKey.user_id == current_user.id)
    for api_key_obj in api_key_obj_list:
        db.delete(api_key_obj)
    db.commit()
