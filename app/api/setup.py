import app.deps
from app.models.user import User, UserSchema, PasswordComplexityError

from fastapi import APIRouter, Depends, HTTPException, status
from pydantic import BaseModel, EmailStr
from sqlalchemy.orm import Session

router = APIRouter()


class SetupSchema(BaseModel):
    email: EmailStr
    full_name: str
    password: str


@router.post("/setup", response_model=UserSchema, status_code=status.HTTP_201_CREATED)
def setup(new_user: SetupSchema, db: Session = Depends(app.deps.get_db)):
    # First ensure no users exist in the DB, if so, do nothing.
    db_user = db.query(User).first()
    if db_user is not None:
        # At least one user already exists, throw an error.
        raise HTTPException(status.HTTP_400_BAD_REQUEST,
                            detail="This instance has already been setup.")

    # There are no users, create a new admin.
    try:
        new_admin = User(
            email=new_user.email,
            full_name=new_user.full_name,
            cleartext_password=new_user.password,
            is_admin=True,
            is_active=True,
            )
        db.add(new_admin)
        db.commit()
        db.refresh(new_admin)
        return new_admin
    except PasswordComplexityError:
        # Password didn't meet requirements, throw an error.
        raise HTTPException(status.HTTP_400_BAD_REQUEST,
                            detail="Passwords need to be at least 10 characters long, and contain a mix of lowercase "
                            "characters, uppercase characters, and digits.")
