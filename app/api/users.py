from app.api.api_keys import create_new_api_key
from app.deps import get_db
from app.models.user import User, UserSchema, PasswordComplexityError
from app.security import get_current_user, get_current_admin
from app.config import settings

from fastapi import APIRouter, Depends, HTTPException, status, Request
from fastapi.responses import StreamingResponse
from pydantic import BaseModel, EmailStr
from typing import List, Optional
from sqlalchemy.exc import SQLAlchemyError
from sqlalchemy.orm import Session
from loguru import logger
import qrcode
import urllib.parse
from io import BytesIO

router = APIRouter()


class OAuth2Token(BaseModel):
    access_token: str
    token_type: str


class NewUserSchema(BaseModel):
    email: EmailStr
    full_name: str
    password: str


class UpdateUserSchema(BaseModel):
    full_name: Optional[str] = None
    password: Optional[str] = None
    is_active: Optional[bool] = None
    is_admin: Optional[bool] = None


@router.get("/users/me/config")
def get_user_me_config(request: Request,
                       current_user: User = Depends(get_current_user),
                       db: Session = Depends(get_db)):
    if settings.SERVER_LOCATION is None or settings.SERVER_LOCATION == "":
        # There a possibility that this autodetected server location could be incorrect if there
        # is a reverse proxy or something in front.
        server_location = str(request.base_url)
    else:
        server_location = settings.SERVER_LOCATION

    new_api_key_obj = create_new_api_key(db, current_user)
    params = {"api_key": new_api_key_obj.cleartext_key}

    # Surely there is a better way to do this....
    config_url = server_location + "?" + urllib.parse.urlencode(params)

    # Create the QR code...
    img = qrcode.make(config_url)
    buf = BytesIO()

    # Cram it in a bytes buffer
    img.save(buf, format="PNG")
    buf.seek(0)

    # Return bytes buffer.
    return StreamingResponse(buf, media_type="image/png")


@router.get("/users/me", response_model=UserSchema)
def get_user_me(current_user: User = Depends(get_current_user)):
    return current_user


# All users can retrieve information any other specific user.
@router.get("/users/{user_id}", response_model=UserSchema)
def get_user(user_id: int,
             current_user: User = Depends(get_current_user),
             db: Session = Depends(get_db)):
    user_obj = db.query(User).filter(User.id == user_id).first()
    if user_obj is None:
        raise HTTPException(status.HTTP_404_NOT_FOUND, detail="User not found.")
    return user_obj


# All users can retrieve information about all other users.
@router.get("/users", response_model=List[UserSchema])
def get_user_list(current_user: User = Depends(get_current_user),
                  db: Session = Depends(get_db)):
    user_objs = db.query(User)
    return list(user_objs)


# Only admins can create users.
@router.post("/users", response_model=UserSchema, status_code=status.HTTP_201_CREATED)
def create_user(new_user: NewUserSchema,
                db: Session = Depends(get_db),
                current_user: User = Depends(get_current_admin)):

    # Ensure the user doesn't already exist in the system
    db_user = db.query(User).filter(User.email == new_user.email).first()
    if db_user is not None:
        # At least one user already exists, throw an error. This is technically a security risk
        # since it allows someone to enumerate users, but it can only be done by an authenticated
        # admin, so it's very low risk.
        raise HTTPException(status.HTTP_400_BAD_REQUEST, detail="A user with that email already exists.")

    # Create a new User. Since it's being manually created, we assume it's active.
    try:
        new_user_obj = User(
            email=new_user.email,
            full_name=new_user.full_name,
            cleartext_password=new_user.password,
            is_admin=False,
            is_active=True,
            )

        db.add(new_user_obj)
        db.commit()
        db.refresh(new_user_obj)
        return new_user_obj
    except PasswordComplexityError:
        # Password didn't meet requirements, throw an error.
        raise HTTPException(status.HTTP_400_BAD_REQUEST,
                            detail="Passwords need to be 10 characters long, and contain a mix of lowercase "
                            "characters, uppercase characters, and digits.")


@router.delete("/users/{user_id}", status_code=status.HTTP_204_NO_CONTENT)
def delete_user(user_id: int,
                db: Session = Depends(get_db),
                current_user: User = Depends(get_current_admin)):
    if user_id == current_user.id:
        raise HTTPException(status.HTTP_404_NOT_FOUND, detail="You cannot delete yourself.")

    user_obj = db.query(User).filter(User.id == user_id).first()
    if user_obj is None:
        raise HTTPException(status.HTTP_404_NOT_FOUND, detail="User not found.")
    try:
        db.delete(user_obj)
        db.commit()
    except SQLAlchemyError as e:
        logger.error(str(e))
        raise HTTPException(status.HTTP_500_INTERNAL_SERVER_ERROR, detail="User deletion failed.")


@router.put("/users/{user_id}", response_model=UserSchema, status_code=status.HTTP_200_OK)
def update_user(user_id: int,
                user_update: UpdateUserSchema,
                db: Session = Depends(get_db),
                current_user: User = Depends(get_current_user)):
    user_obj = db.query(User).filter(User.id == user_id).first()
    if user_obj is None:
        # Disallow updating others profiles, unless you're an admin.
        raise HTTPException(status.HTTP_404_NOT_FOUND, detail="User not found.")
    elif not current_user.is_admin and user_id != current_user.id:
        raise HTTPException(status.HTTP_403_FORBIDDEN, detail="Administrator permissions are required to update other "
                            "profiles.")

    if not current_user.is_admin:
        # There are certain parts of a user only admins can update (a user should not be able to
        # make themselves an admin).
        if user_update.is_admin is not None or user_update.is_active is not None:
            raise HTTPException(status.HTTP_403_FORBIDDEN, detail="You are not permitted to update "
                                "some fields of your user profile. Please contact your administrator.")

    try:
        if user_update.full_name is not None:
            user_obj.full_name = user_update.full_name

        if user_update.password is not None:
            try:
                user_obj.set_password(user_update.password)
            except PasswordComplexityError:
                # Password didn't meet requirements, throw an error.
                raise HTTPException(status.HTTP_400_BAD_REQUEST,
                                    detail="Passwords need to be 10 characters long, and contain a mix of lowercase "
                                    "characters, uppercase characters, and digits.")

        db.commit()
        db.refresh(user_obj)
    except SQLAlchemyError as e:
        logger.error(str(e))
        raise HTTPException(status.HTTP_500_INTERNAL_SERVER_ERROR, detail="User update failed.")
    return user_obj
