from app.deps import get_db
from app.config import settings
from app.models.asset_tag import AssetTag
from app.models.item import Item
from app.models.user import User
from app.models.api_key import ApiKey

from fastapi import status, HTTPException, Depends, Header
from fastapi.security import OAuth2PasswordBearer
from fastapi.security.utils import get_authorization_scheme_param
from starlette.requests import Request
from typing import Optional
from sqlalchemy.orm import Session
from datetime import datetime, timedelta
from jose import jwt, JWTError
import hashlib


class WhereTFisOAuth2PasswordBearer(OAuth2PasswordBearer):
    async def __call__(self, request: Request) -> Optional[str]:
        # Slightly modified from https://github.com/tiangolo/fastapi/blob/master/fastapi/security/oauth2.py
        try:
            authorization: str = request.headers.get("Authorization")
            scheme, param = get_authorization_scheme_param(authorization)
            if not authorization or scheme.lower() != "bearer":
                return None
            return param
        except KeyError:
            return None


oauth2_scheme = WhereTFisOAuth2PasswordBearer(tokenUrl="api/login")


def create_token(user: str,
                 expires: timedelta = timedelta(minutes=60)):
    decoded_token = {"user": user.to_dict()}
    token_expire_time = datetime.utcnow() + expires
    decoded_token["exp"] = token_expire_time

    return jwt.encode(decoded_token, settings.SECRET, algorithm="HS256")


# This function ensures that any API endpoint which has this as an injected dependency (e.g.
# Depends(get_current_user)) is only callable by an authenticated user, with no additional privilege
# checks. This is approximately the same as a @requires_login decorator in Flask.
def get_current_user(db: Session = Depends(get_db),
                     token: Optional[str] = Depends(oauth2_scheme),
                     x_api_key: Optional[str] = Header(None)
                     ) -> User:
    credentials_exception = HTTPException(
        status_code=status.HTTP_401_UNAUTHORIZED,
        detail="Could not validate credentials",
        headers={"WWW-Authenticate": "Bearer"},
    )
    # We can authenticate with either the WWW-Authenticate header, or the X-API-Key header.
    if (token is None and x_api_key is None) or (token is not None and x_api_key is not None):
        raise credentials_exception

    user_obj = None

    if token:
        # Authenticate via Oauth bearer token (username + password)
        try:
            payload = jwt.decode(token, settings.SECRET, algorithms=["HS256"])
            if payload["user"]["email"] is None:
                raise credentials_exception

            user_obj = db.query(User).filter(User.email == payload["user"]["email"]).first()
        except JWTError:
            raise credentials_exception

    elif x_api_key:
        # Authenticate via API Key
        sent_key_stub = x_api_key[-4:]
        # We search via key_stub first just to remove 99% of all other possible API keys, because verifying an API 
        # key is costly, and to try every single one would be stupid. It's unlikely there will ever be multiple 
        # stubs that match, but who knows.
        api_key_obj_list = db.query(ApiKey).filter(ApiKey.key_stub == sent_key_stub).all()
        for api_key_obj in api_key_obj_list:
            if api_key_obj.is_key_correct(x_api_key):
                # The API key is correct, set the user for the session.
                user_obj = db.query(User).filter(User.id == api_key_obj.user_id).first()
                break

    else:
        # No idea how we got here.
        raise credentials_exception

    if user_obj is None:
        raise credentials_exception
    # Also ensure only active users can login.
    if not user_obj.is_active:
        raise HTTPException(status_code=status.HTTP_403_FORBIDDEN,
                            detail="Account is not active. Please contact your administrator.")

    return user_obj


# Ensures the authenticated user is an admin.
def get_current_admin(db: Session = Depends(get_db),
                      user: User = Depends(get_current_user)
                      ) -> User:
    admin_user_obj = db.query(User).filter(User.id == user.id).filter(User.is_admin.is_(True)).first()
    if admin_user_obj is None:
        raise HTTPException(status.HTTP_403_FORBIDDEN, detail="Administrator permissions are required.")

    return admin_user_obj


# This will make sure a user has access (of any level) to the item they are attempting to access.
def get_item_obj_by_asset_tag(
        asset_tag_id: str,
        db: Session = Depends(get_db)) -> Item:
    
    asset_tag_obj = db.query(AssetTag).filter(AssetTag.id == asset_tag_id).first()
    if asset_tag_obj is None:
        raise HTTPException(status.HTTP_404_NOT_FOUND, detail=f"Asset tag '{asset_tag_id}' not found.")

    # Get item object.
    item_obj = db.query(Item).filter(Item.id == asset_tag_obj.item_id).first()
    if item_obj is None:
        raise HTTPException(status.HTTP_404_NOT_FOUND, detail=f"Item ID '{asset_tag_obj.item_id}' not found. This should not happen, and is likely a backend bug.")
    return item_obj