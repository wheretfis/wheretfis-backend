from pydantic import BaseSettings

import app.utils
import secrets
import string

default_secret = "".join(secrets.choice(string.ascii_letters + string.digits) for i in range(32))


# Pydantic will import from ENV vars automatically, or use the defaults here
class Settings(BaseSettings):
    DEBUG: bool = False
    PORT: int = 5000
    # This represents the server location of where the WhereTFisserver si running.
    # Example: https://wheretfis.mynetwork.local:443/
    SERVER_LOCATION: str = ""
    LOG_LEVEL: str = "WARNING"
    LOG_TIMESTAMP: bool = True
    DATABASE_URL: str = app.utils.get_local_db_location()
    SECRET: str = default_secret


settings = Settings()
