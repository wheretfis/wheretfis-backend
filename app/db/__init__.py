from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

from app.config import settings

# The check_same_thread arg is only needed for SQLite DBs.
tmp_args = {}
if "sqlite://" in settings.DATABASE_URL:
    tmp_args = {"check_same_thread": False}
engine = create_engine(settings.DATABASE_URL, connect_args=tmp_args)
SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)
