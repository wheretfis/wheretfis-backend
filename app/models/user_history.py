from sqlalchemy import Column, Integer, DateTime
from sqlalchemy.sql import func
from pydantic import BaseModel
from datetime import datetime
from typing import Optional

from app.db.base_class import Base


class UserHistorySchema(BaseModel):
    id: str
    item_id: int
    new_user_id: Optional[int]
    creation_time: datetime

    class Config:
        orm_mode = True


class UserHistory(Base):
    __tablename__ = "user_history"

    id = Column(Integer(), primary_key=True, index=True)
    item_id = Column(Integer(), nullable=False)
    new_user_id = Column(Integer(), nullable=False)
    creation_time = Column(DateTime(), server_default=func.now())

    def __init__(
        self,
        item_id: int,
        new_user_id: int,
    ):
        self.item_id = item_id
        self.new_user_id = new_user_id
        # Creation time is set automatically by the DB

    def to_dict(self):
        return UserHistorySchema.from_orm(self).dict()
