from sqlalchemy import Boolean, Column, Integer, String
from passlib.hash import bcrypt
from pydantic import BaseModel, EmailStr
import re

from app.db.base_class import Base


class PasswordComplexityError(Exception):
    pass


class ShortUserSchema(BaseModel):
    id: int
    full_name: str

    class Config:
        orm_mode = True


class UserSchema(BaseModel):
    id: int
    full_name: str
    email: EmailStr
    is_active: bool

    class Config:
        orm_mode = True


class User(Base):
    __tablename__ = "users"

    id = Column(Integer(), primary_key=True, index=True)
    full_name = Column(String(), index=True)
    email = Column(String(), unique=True, index=True, nullable=False)
    # This is hashed. Always.
    _password = Column(String(60), nullable=False)
    is_active = Column(Boolean(), default=True)
    is_admin = Column(Boolean(), default=False)

    def __init__(
        self,
        email: str,
        cleartext_password: str,
        full_name: str = "",
        is_active: bool = True,
        is_admin: bool = False,
    ):
        self.email = email
        self.full_name = full_name
        self.is_active = is_active
        self.is_admin = is_admin
        self.set_password(cleartext_password)

    def set_password(self, cleartext_password: str):
        # Check password complexity

        # First remove any whitespace
        cleartext_password = cleartext_password.strip()

        # Passwords must be at least 10 characters long
        if len(cleartext_password) < 10:
            raise PasswordComplexityError()

        # Passwords must contain at least one lowercase character
        if not re.search(r"[a-z]", cleartext_password):
            raise PasswordComplexityError()

        # Passwords must contain at least one uppercase character
        if not re.search(r"[A-Z]", cleartext_password):
            raise PasswordComplexityError()

        # Passwords must contain at least one digit
        if not re.search(r"[0-9]", cleartext_password):
            raise PasswordComplexityError()

        # Passwords should not contain whitespace
        if re.search(r"[\s]", cleartext_password):
            raise PasswordComplexityError()

        self._password = bcrypt.hash(cleartext_password)

    def is_password_correct(self, cleartext_password_to_try: str) -> bool:
        # First remove any whitespace
        cleartext_password_to_try = cleartext_password_to_try.strip()
        return bcrypt.verify(cleartext_password_to_try, self._password)

    def to_dict(self):
        return UserSchema.from_orm(self).dict()
