from sqlalchemy import Column, Integer, String, DateTime
from sqlalchemy.sql import func
from pydantic import BaseModel
from datetime import datetime
from typing import Optional

from app.db.base_class import Base


class LocationHistorySchema(BaseModel):
    id: int
    item_id: int
    new_container_item_id: Optional[int]
    new_latitude: Optional[str]
    new_longitude: Optional[str]
    creation_time: datetime

    class Config:
        orm_mode = True


class LocationHistory(Base):
    __tablename__ = "location_history"

    id = Column(Integer(), primary_key=True, index=True)
    item_id = Column(Integer(), nullable=False)
    new_container_item_id = Column(Integer(), nullable=True)
    new_latitude = Column(String(), nullable=True)
    new_longitude = Column(String(), nullable=True)
    creation_time = Column(DateTime(), server_default=func.now())

    def __init__(
        self,
        item_id: int,
        new_container_item_id: int = None,
        new_latitude: str = None,
        new_longitude: str = None
    ):
        self.item_id = item_id
        self.new_container_item_id = new_container_item_id
        self.new_latitude = new_latitude
        self.new_longitude = new_longitude
        # Creation time is set automatically by the DB

    def to_dict(self):
        return LocationHistorySchema.from_orm(self).dict()
