from sqlalchemy import Column, Integer, String, DateTime
from sqlalchemy.sql import func
from pydantic import BaseModel
from datetime import datetime
from typing import Optional
import string
import secrets
from passlib.hash import bcrypt

from app.db.base_class import Base

# Loci Notes API Key Identifier
API_KEY_IDENTIFIER = "WHERETFISAPI_"


class ApiKeySchema(BaseModel):
    id: int
    user_id: int
    cleartext_key: Optional[str]
    creation_time: datetime
    key_stub: str

    class Config:
        orm_mode = True


class ApiKey(Base):
    __tablename__ = "api_keys"

    id = Column(Integer(), primary_key=True, index=True)
    user_id = Column(Integer(), nullable=False)
    # bcrypted key. The unique=True part is legacy so I don't have to alter the table.
    _key = Column(String(80), unique=True, index=True, nullable=False)
    creation_time = Column(DateTime(), server_default=func.now())
    # The key stub is a 4 digit identifier of the last 4 digits of the cleartext key so that users can
    # identify which key is which.
    key_stub = Column(String(), nullable=True)

    def __init__(
        self,
        user_id: int,
    ):
        self.user_id = user_id
        # This variable, cleartext_key, is only set in the constructor, so the only way to know what it is is at
        # creation time.
        self.cleartext_key = API_KEY_IDENTIFIER + \
            "".join(secrets.choice(string.ascii_letters + string.digits) for i in range(32))
        self._key = bcrypt.hash(self.cleartext_key)
        self.key_stub = self.cleartext_key[-4:]

    def is_key_correct(self, cleartext_key_to_try: str) -> bool:
        # First remove any whitespace
        cleartext_key_to_try = cleartext_key_to_try.strip()
        return bcrypt.verify(cleartext_key_to_try, self._key)

    def to_dict(self):
        return ApiKeySchema.from_orm(self).dict()
