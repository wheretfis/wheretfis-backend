from sqlalchemy import Column, String, Integer
from pydantic import BaseModel

from app.db.base_class import Base


class AssetTagSchema(BaseModel):
    id: str
    item_id: int

    class Config:
        orm_mode = True


class AssetTag(Base):
    __tablename__ = "asset_tags"

    id = Column(String(), primary_key=True, index=True)
    item_id = Column(Integer(), nullable=False)

    def __init__(
        self,
        id: str,
        item_id: int,
    ):
        self.id = id
        self.item_id = item_id

    def to_dict(self):
        return AssetTagSchema.from_orm(self).dict()
