from sqlalchemy import Column, String, DateTime, Boolean, Integer
from sqlalchemy.sql import func
from pydantic import BaseModel
from datetime import datetime
from typing import List, Optional

from app.db.base_class import Base
from app.models.user import ShortUserSchema


class ShortItemSchema(BaseModel):
    id: Optional[int]
    description: Optional[str]
    asset_tags: List[str]

    class Config:
        orm_mode = True


class ItemSchema(BaseModel):
    id: int
    description: Optional[str]
    notes: Optional[str]
    is_container: bool
    is_physical_place: bool
    creation_time: datetime
    picture_data: Optional[str]
    last_user: Optional[ShortUserSchema]
    parent_container: Optional[ShortItemSchema]
    is_active: bool
    asset_tags: Optional[List[str]]

    class Config:
        orm_mode = True


class NewItemSchema(BaseModel):
    description: Optional[str]
    parent_container_asset_tag_id: Optional[str]
    notes: Optional[str]
    is_container: bool
    is_physical_place: bool
    picture_data: Optional[str]
    latitude: Optional[str]
    longitude: Optional[str]
    asset_tags: List[str]

    class Config:
        orm_mode = True


class UpdateItemSchema(BaseModel):
    description: Optional[str]
    notes: Optional[str]
    picture_data: Optional[str]
    asset_tags: Optional[List[str]]

    class Config:
        orm_mode = True


class UpdateItemLocationSchema(BaseModel):
    parent_container_asset_tag_id: Optional[str]
    latitude: Optional[str]
    longitude: Optional[str]

    class Config:
        orm_mode = True


class Item(Base):
    __tablename__ = "items"

    id = Column(Integer(), primary_key=True, index=True)
    description = Column(String(), nullable=True)
    notes = Column(String(), nullable=True)
    is_container = Column(Boolean(), default=False)
    is_physical_place = Column(Boolean(), default=False)
    picture_data = Column(String(), nullable=True)
    creation_time = Column(DateTime(), server_default=func.now())
    # Instead of deleting items, we only mark them inactive, so they can't used, and movement histories are retained.
    # Inactive items are not allowed to be updated, but can be retrieved.
    is_active = Column(Boolean(), default=True)

    def __init__(
        self,
        is_container: bool = False,
        is_physical_place: bool = False,
        description: str = None,
        notes: str = None,
        picture_data: str = None
    ):
        self.description = description
        self.notes = notes
        self.is_container = is_container
        self.is_physical_place = is_physical_place
        self.picture_data = picture_data
        # Creation time is set automatically by the DB
        self.is_active = True

    def to_dict(self):
        return ItemSchema.from_orm(self).dict()
