import pathlib
import os


# This function from https://github.com/tensorflow/tensorboard/blob/master/tensorboard/uploader/util.py
# Apache 2 licensed
def get_user_config_directory():
    """Returns a platform-specific root directory for user config settings."""
    # On Windows, prefer %LOCALAPPDATA%, then %APPDATA%, since we can expect the
    # AppData directories to be ACLed to be visible only to the user and admin
    # users (https://stackoverflow.com/a/7617601/1179226). If neither is set,
    # return None instead of falling back to something that may be world-readable.
    if os.name == "nt":
        appdata = os.getenv("LOCALAPPDATA")
        if appdata:
            return appdata
        appdata = os.getenv("APPDATA")
        if appdata:
            return appdata
        return None
    # On non-windows, use XDG_CONFIG_HOME if set, else default to ~/.config.
    xdg_config_home = os.getenv("XDG_CONFIG_HOME")
    if xdg_config_home:
        return xdg_config_home
    return os.path.join(os.path.expanduser("~"), ".config")


def get_wheretfis_dir() -> str:
    wheretfis_dir = pathlib.Path(get_user_config_directory(), "wheretfis")
    # Create the directory if it doesn't exist yet.
    wheretfis_dir.mkdir(parents=True, exist_ok=True)
    return str(wheretfis_dir)


def get_local_db_location():
    return "sqlite:///" + str(pathlib.Path(get_wheretfis_dir(), "wheretfis-db.sqlite"))
