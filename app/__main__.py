import sys
import logging
import pydantic
from loguru import logger
from fastapi import FastAPI
from uvicorn import Config, Server

from app import __version__
from app.api.main import api_router


# First, setup all logging to be unified under loguru.
class LoggingInterceptHandler(logging.Handler):
    def emit(self, record):
        # Get corresponding Loguru level if it exists
        try:
            level = logger.level(record.levelname).name
        except ValueError:
            level = record.levelno

        # Find caller from where originated the logged message
        frame, depth = logging.currentframe(), 2
        while frame.f_code.co_filename == logging.__file__:
            frame = frame.f_back
            depth += 1

        logger.opt(depth=depth, exception=record.exc_info).log(level, record.getMessage())


# From https://pawamoy.github.io/posts/unify-logging-for-a-gunicorn-uvicorn-app/
def setup_logging():
    # intercept everything at the root logger
    logging.root.handlers = [LoggingInterceptHandler()]
    logging.root.setLevel(settings.LOG_LEVEL)

    # remove every other logger's handlers
    # and propagate to root logger
    for name in logging.root.manager.loggerDict.keys():
        logging.getLogger(name).handlers = []
        logging.getLogger(name).propagate = True

    # configure loguru
    logger.configure(handlers=[{"sink": sys.stdout}])


try:
    from app.config import settings
except pydantic.error_wrappers.ValidationError as e:
    for err in e.errors():
        logger.error("The ENV variable '%s' is not set correctly." % err["loc"])
    quit()

app = FastAPI(title="WhereTFis Server",
              description="The WhereTFis backend",
              version=__version__)
app.include_router(api_router, prefix="/api")


if __name__ == "__main__":
    config = Config(
        "app.__main__:app",
        host="127.0.0.1" if settings.DEBUG else "0.0.0.0",
        port=settings.PORT,
        log_level=settings.LOG_LEVEL.lower(),
        debug=settings.DEBUG,
        reload=settings.DEBUG,
        log_config={
            "version": 1,
            'handlers': {
                "default": {
                    "class": "app.__main__.LoggingInterceptHandler",
                },
            },
            "loggers": {
                "": {
                    "handlers": ["default"],
                    "propagate": False
                },
                "fastapi": {
                    "handlers": ["default"],
                    "propagate": False
                },
                "uvicorn": {
                    "handlers": ["default"],
                    "propagate": False
                },
                "watchgod": {
                    "handlers": ["default"],
                    "propagate": False
                },
                "__main__": {
                    "handlers": ["default"],
                    "propagate": False
                }
            },
        })
    server = Server(config=config)

    if config.should_reload:
        from uvicorn.supervisors import ChangeReload

        sock = config.bind_socket()
        supervisor = ChangeReload(config, target=server.run, sockets=[sock])

        supervisor.run()
    else:
        setup_logging()
        server.run()
