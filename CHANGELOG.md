# Changelog

<!--next-version-placeholder-->

## v0.4.1 (2022-04-27)
### Fix
* Add SERVER_LOCATION for better autoconfig ([`0f06812`](https://gitlab.com/wheretfis/wheretfis-backend/-/commit/0f068120d807cf703c6dab3334baee5d7c05ab74))

## v0.4.0 (2022-04-17)
### Feature
* Multi-tagging of items ([`e58c094`](https://gitlab.com/wheretfis/wheretfis-backend/-/commit/e58c094989d752223a961dca28d58d4e4b25e09f))
* Return a container object with `get_item_full`, not just an id ([`2a9bdd2`](https://gitlab.com/wheretfis/wheretfis-backend/-/commit/2a9bdd22bdc965a3d436d4e8905f55139279c493))
* Change update_item_location to return the current object ([`388704d`](https://gitlab.com/wheretfis/wheretfis-backend/-/commit/388704da68e81c4c5a505c0f661bf122430380c7))

### Fix
* Allow photos to be nulled out ([`253a236`](https://gitlab.com/wheretfis/wheretfis-backend/-/commit/253a2367fd04fca8d2d058a2e73e3f3592651333))
* Correct a bad default when migrating ([`bc8ea13`](https://gitlab.com/wheretfis/wheretfis-backend/-/commit/bc8ea13692ef4503e7966b2263d1f0b0d0e0c625))
* Change delete_item from returning 204 to 200 ([`f869a2d`](https://gitlab.com/wheretfis/wheretfis-backend/-/commit/f869a2dbac7fb29705ebc8e894288f3c6c659ea8))

## v0.3.0 (2022-04-07)
### Feature
* Added item editing ([`822c8b6`](https://gitlab.com/wheretfis/wheretfis-backend/-/commit/822c8b6b0e65741e1e3d54109e42273906d7a53a))
* Item deletion ([`228b541`](https://gitlab.com/wheretfis/wheretfis-backend/-/commit/228b541d3b858b241d146ae09e7bde1f734a8d50))
* Rework API key management ([`1cdebec`](https://gitlab.com/wheretfis/wheretfis-backend/-/commit/1cdebecf16064c13b6d811c7e1d49cd8caec5056))

## v0.2.2 (2022-01-21)
### Fix
* Alembic db length upgrade ([`f7d022f`](https://gitlab.com/wheretfis/wheretfis-backend/-/commit/f7d022facc3c0604dbd1fa3259ad891ef53ecaaa))

## v0.2.1 (2022-01-21)
### Fix
* Resolve api_key sql length error ([`b5cdb99`](https://gitlab.com/wheretfis/wheretfis-backend/-/commit/b5cdb999fd33257064e9b08c2ebcf8328ac747ba))

## v0.2.0 (2022-01-21)
### Feature
* Autoconfig via qr code ([`2951ef4`](https://gitlab.com/wheretfis/wheretfis-backend/-/commit/2951ef4e9ed742e1cf98addbcc4da8f6337e78d4))

### Fix
* Correct Docker deployment problems ([`c4968bb`](https://gitlab.com/wheretfis/wheretfis-backend/-/commit/c4968bb4cd3f66dfe5ef004c41bcc28e94abd928))

## v0.1.2 (2022-01-02)
### Fix
* Correct the CICD release process ([`47f2e89`](https://gitlab.com/wheretfis/wheretfis-backend/-/commit/47f2e89ea27df025ae44729b96262192442ec6ae))

## v0.1.1 (2022-01-02)
### Fix
* Require login for get_item_list API call ([`a30e0b7`](https://gitlab.com/wheretfis/wheretfis-backend/-/commit/a30e0b7f2666c50b0024eefd26c15435945472f6))

## v0.1.0 (2022-01-02)
### Feature
* Improved item query capbilities ([`d1629f0`](https://gitlab.com/wheretfis/wheretfis-backend/-/commit/d1629f06b584b56ba818f6a91043ada91d588adf))
* Add is_location flag to items ([`616314e`](https://gitlab.com/wheretfis/wheretfis-backend/-/commit/616314e48f343f365cdac3652b96febf807a2394))
* Accept null location ID updates ([`30eaf8d`](https://gitlab.com/wheretfis/wheretfis-backend/-/commit/30eaf8daeaa5a59389a582c5b0af5f06eefc76e4))
* Add docker deployment ([`4823415`](https://gitlab.com/wheretfis/wheretfis-backend/-/commit/4823415339c4bf02f43d2c7177ab3dcd1583c9cf))

### Fix
* Prevent forward slashes in IDs ([`3101198`](https://gitlab.com/wheretfis/wheretfis-backend/-/commit/3101198e8db95172dab92f57a9b8af80bd657b4b))
* Remove vscode dir ([`43ece05`](https://gitlab.com/wheretfis/wheretfis-backend/-/commit/43ece0534bafbe1aaa79294a8b5fe179f7a18958))
* Corrected bug where new items have null parents ([`1f8b9d2`](https://gitlab.com/wheretfis/wheretfis-backend/-/commit/1f8b9d2c8061af59ec3e7c493f3f162d6f14adda))
* Container and place validation checks ([`c9ace34`](https://gitlab.com/wheretfis/wheretfis-backend/-/commit/c9ace34c88db9f98dc583677a4ec07e8a0440c05))

